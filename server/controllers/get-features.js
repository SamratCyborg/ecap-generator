﻿module.exports = function (app, route) {
    return function (req, res, next) {
        getFeatures(req.body).then(function(response) {
            res.send(response);
            next();
        }, function(err) {
            res.send(err);
            next();
        })
    };

    function getFeatures(appDetails) {
        var Q = require('q');
        var defered = Q.defer();
        getAppSourcePath().then(function (res) {
            var appBaseDir = res + (res.charAt(res.length - 1) == '/' ? 'app' : '/app')+ getAppPath(appDetails);;

            var appFeaturesJsonPath = appBaseDir + '/' + appDetails.appName + '.features.json';
            var appJsonPath = appBaseDir + '/' + appDetails.appName + '.json';

            var appFeaturesData = readJsonFile(appFeaturesJsonPath);
            var appJsonData = readJsonFile(appJsonPath);
            var features;
            extractFeatures(appFeaturesData.features, appJsonData.featureMap, appDetails).then(function (res) {
                features = res;
                defered.resolve(features);
            }, function(err) {
                defered.reject(err);
            });
        }, function (err) {
            defered.reject(err);
        })

        return defered.promise;
    }
    function getAppSourcePath() {
        var Q = require('q');
        var deferred = Q.defer();

        var cache = require('../providers/node-cache-provider');
        cache.instance().get("connectionDetails", function (err, value) {
            if (!err && value) {
                value.src = value.src.replace(/\\/g, '/');
                value.src = value.src.replace('\\', '/');
                deferred.resolve(value.src);
            } else {
                deferred.reject({
                    status: false,
                    msg: "No connection data available"
                });

            }
        });

        return deferred.promise;
    }

    function extractFeatures(appFeatureData, appJsonData, appDetails) {
        var Q = require('q');
        var defered = Q.defer();
        var endpointMaps = {};
        var roleMaps = {};
        var models = prepareModels();
       
        getEndpoints(appDetails, models.featureEndpointMapModel).then(function (res) {
            endpointMaps = res;
            getRoles(appDetails, models.featureRoleMapModel).then(function (res) {
                roleMaps = res;
                var features = [];
                for (var i = 0; i < appFeatureData.length; i++) {
                    if (appJsonData[appFeatureData[i].id]) {
                        var feature = {
                            "featureId": appFeatureData[i].id,
                            "featureName":appFeatureData[i].name,
                            "views": appJsonData[appFeatureData[i].id].views,
                            "url": appJsonData[appFeatureData[i].id].url,
                            "icon": appFeatureData[i].icon
                        }
                        feature.endpoints = endpointMaps[appFeatureData[i].id] || [];
                        feature.roles = roleMaps[appFeatureData[i].id]|| [];
                        features.push(feature);
                    }
                }

                defered.resolve(features);
            }, function(err) {
                console.log(err);
                defered.reject(err);
            })
        }, function(err) {
            console.log(err);
            defered.reject(err);
        })
        return defered.promise;
    }
    function getEndpoints(appDetails, model) {
       
        var filter = {
            AppName: appDetails.appName,
            AppType: appDetails.appType == ("business" || appDetails.appType =="component")?appDetails.appType + '.'+appDetails.siteName: appDetails.appType
        }
        var Q = require('q');
        var defered = Q.defer();
        var endpoints = {};
        connectToDb().then(function (response) {
            // var femModel = prepareModels().featureEndpointMapModel;
            model.find(filter, function (err, result) {
                if (err) {
                    console.log(err);
                    defered.reject(err);
                } else {
                    for (var i = 0; i < result.length; i++) {
                        if (endpoints[result[i].FeatureId] && Array.isArray(endpoints[result[i].FeatureId]))
                            endpoints[result[i].FeatureId].push(result[i].ResourcePath);
                        else {
                            endpoints[result[i].FeatureId] = [result[i].ResourcePath]
                        }
                    }

                    defered.resolve(endpoints);
                }
            });
           
        }, function(error) {
            defered.reject(error);
        });
        return defered.promise;
    }
    function getRoles(appDetails, model) {
        var filter = {
            AppName: appDetails.appName,
            AppType: appDetails.appType == ("business" || appDetails.appType == "component") ? appDetails.appType + '.' + appDetails.siteName : appDetails.appType
        }
        var Q = require('q');
        var defered = Q.defer();
        
        var roles = {};
        connectToDb().then(function (response) {
            
            //var remModel = prepareModels().featureRoleMapModel;
            model.find(filter, function (err, result) {
                if (err) {
                    console.log(err);
                    defered.reject(err);
                } else {
                    for (var i = 0; i < result.length; i++) {
                        //roles[result[i].FeatureId] = result[i].RoleName;
                        if (roles[result[i].FeatureId] && Array.isArray(roles[result[i].FeatureId]))
                            roles[result[i].FeatureId].push(result[i].RoleName);
                        else {
                            roles[result[i].FeatureId] = [result[i].RoleName]
                        }
                    }
                    defered.resolve(roles);
                }
            });

           
        }, function (error) {
            defered.reject(error);
        });
      
        return defered.promise;
    }
    function prepareModels() {
        var mongoose = require('mongoose');
        var schema = mongoose.Schema;
        var featureEndpointMapModel;
        var featureRoleMapModel
        var featureEndpointMapSchema = new schema({
            AppType: {
                type: String,
                required: true
            },
            AppName: {
                type: String,
                required: true
            },
            FeatureId: {
                type: String,
                required: true
            },
            FeatureName: {
                type: String
            },
            ResourceId: {
                type: String,
                required: true
            },
            Service: {
                type: String,
                required: true
            },
            Controller: {
                type: String,
                required: true
            },
            Action: {
                type: String,
                required: true
            },
            ResourcePath: {
                type: String,
                required: true
            }
        }, { collection: 'FeatureEndPointMaps' });
        var featureRoleMapSchema = new schema({
            AppType: {
                type: String,
                required: true
            },
            AppName: {
                type: String,
                required: true
            },
            FeatureId: {
                type: String,
                required: true
            },
            FeatureName: {
                type: String,
                required: true
            },
            RoleName: {
                type: String,
                required: true
            }
        }, { collection: 'FeatureRoleMaps' });
        try {
            featureEndpointMapModel = mongoose.model('FeatureEndPointMap')
        } catch (error) {
            featureEndpointMapModel = mongoose.model("FeatureEndPointMap", featureEndpointMapSchema);
        }
        try {
            featureRoleMapModel = mongoose.model('FeatureRoleMap')
        } catch (error) {
            featureRoleMapModel = mongoose.model("FeatureRoleMap", featureRoleMapSchema);
        }
        return {
            featureEndpointMapModel: featureEndpointMapModel,
            featureRoleMapModel: featureRoleMapModel
        }
    }
    function connectToDb() {
        var mongoose = require('mongoose');
        var Q = require('q');
        var defered = Q.defer();
        if (!mongoose.connection.readyState) {
            var cache = require('../providers/node-cache-provider');
            cache.instance().get("connectionDetails", function(err, value) {
                if (!err && value) {
                    // console.log("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName);

                    mongoose.connect("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName, function(err) {
                       if (err) {
                           defered.reject({
                               status: false,
                               msg: "Wrong connection details provided",
                               err: err
                           });
                       } else {
                           defered.resolve({
                               status: true,
                               msg: "Connected to db"
                           });
                       }
                    });
                } else {
                    defered.reject({
                        status: false,
                        msg: "No connection data available"
                    });
                   
                }
            });
        } else {
            defered.resolve({
                status: true,
                msg: "Connected to db"
            });
        }
          
       
        return defered.promise;
    }
    function closeMongooseConnection() {
        var mongoose = require('mongoose');
        mongoose.connection.close();
    }
    function getAppPath(appDetails) {
        if (appDetails.appType == "platform" || appDetails.appType == "infrastructure") {
            return '/' + appDetails.appType + '/' + appDetails.appName;
        } else if (appDetails.appType == "business" || appDetails.appType == "component") {
            return '/' + appDetails.appType + "/" + appDetails.siteName + '/' + appDetails.appName;
        }
    }

    function getPlatformOrInfrastructureApps(path) {
        var apps = "";
        if (dirOrFileExists(path)) {
            apps = getDirectories(path);
        }
        return apps;
    }

    function getBusinessOrComponentApps(path) {
        var sites = getDirectories(path);
        var apps = {};

        for (var i = 0; i < sites.length; i++) {
            apps[sites[i]] = getDirectories(path + '/' + sites[i]);
        }
        return apps;
    }

    function getDirectories(srcpath) {
        const fs = require('fs');
        const path = require('path');
        return fs.readdirSync(srcpath)
            .filter(function (file){
                return fs.statSync(path.join(srcpath, file)).isDirectory();
            });
    }


    function dirOrFileExists(path) {
        var fs = require('fs');
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    }

    function readJsonFile(path) {
        var fs = require('fs');
        var rawJsonData = fs.readFileSync(path, 'utf8');
        var jsonData = parseJsonString(rawJsonData);

        if (jsonData) {
            return jsonData;
        } else {
           // console.log("Invalid JSON, " + path);
            console.log("Aborting task....");
            throw new Error("Invalid json supplied");
        }
    }

    function parseJsonString(str) {
        var parsedJson;
        try {
            parsedJson = JSON.parse(str.toString().trim());
        } catch (e) {
            return false;
        }
        return parsedJson;
    }

    

};
