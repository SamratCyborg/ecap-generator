﻿module.exports = function (app, route) {
    return function (req, res, next) {
        connectToDb().then(function (response) {
            if (response.status) {
                res.send(response);
                next();
            } else {
                res.send({status: false});
                next();
            }
            
        }, function(error) {
            res.send(error);
            next();
        });
        
    };

    

    function connectToDb() {
        var mongoose = require('mongoose');
        var Q = require('q');
        var defered = Q.defer();
        if (!mongoose.connection.readyState) {
            var cache = require('../providers/node-cache-provider');
            cache.instance().get("connectionDetails", function (err, value) {
                if (!err && value) {
                    // console.log("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName);

                    mongoose.connect("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName, function (err) {
                        if (err) {
                            defered.reject({
                                status: false,
                                msg: "Wrong connection details provided",
                                err: err
                            });
                        } else {
                            defered.resolve({
                                status: true,
                                msg: "Connected to db"
                            });
                        }
                    });
                } else {
                    defered.reject({
                        status: false,
                        msg: "No connection data available"
                    });

                }
            });
        } else {
            defered.resolve({
                status: true,
                msg: "Connected to db"
            });
        }


        return defered.promise;
    }
};
