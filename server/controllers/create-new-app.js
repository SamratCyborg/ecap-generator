﻿module.exports = function (app, route) {
    return function (req, res, next) {
        res.send(createApp(req.body));
        next();

    };

    function getApps() {
        var appBaseDir = process.cwd() + '/../../src/app';
        var allApps = {
            "platform": getPlatformOrInfrastructureApps(appBaseDir + '/platform'),
            "business": getBusinessOrComponentApps(appBaseDir + '/business'),
            "component": getBusinessOrComponentApps(appBaseDir + '/component'),
            "infrastructure": getPlatformOrInfrastructureApps(appBaseDir + '/infrastructure')
        }

        return allApps;
    }

    function createApp(appDetails) {
        var shell = require("shelljs");
        var npmPath = shell.exec("npm config get prefix");
        var path = require('path');

        var s = npmPath.split(path.sep);
        var baseTemplatePath = "";
        for (var i=0;i< (s.length-1);i++){
            baseTemplatePath += (s[i] + "\\")
        }
        baseTemplatePath += "npm\\node_modules\\ecap-generator\\server";
        baseTemplatePath = baseTemplatePath.replace(/\\/g, '/');


        var outputAppPath;
        var fs = require('fs');
        var appName = appDetails.appName;
        var appUrl = (appDetails.appType == "business" || appDetails.appType == "component") ?
        appDetails.appType + "/" + appDetails.siteName + "/" + appName : appDetails.appType + "/" + appName;
        var appNamewithPrefix = appDetails.appType == "business" || appDetails.appType == "component" ?
        appDetails.siteName + "." + appName : appName;
        var files = {
            controller: [appName],
            service: [appName],
            config: [appName],
            view: [appName],
            style: [appName],
            json: [appName + ".dev", appName, appName + ".features"],
        };

        var variables = {
            appName: appName,
            appNameUpperCase: appName.toUpperCase().replace("-","_"),
            appUrl: appUrl,
            appNameWithPrefix: appNamewithPrefix
        }
        getAppSourcePath().then(function (res) {
            outputAppPath = res + (res.charAt(res.length - 1) == '/' ? 'app/' : '/app/') + appUrl;
            createDirectory(outputAppPath);

            copyi18n();
            createDirectory(outputAppPath + "/controller");
            createDirectory(outputAppPath + "/style");
            createDirectory(outputAppPath + "/view");
            createDirectory(outputAppPath + "/service");
            for(var item in files){
                files[item].forEach(function(fileName){
                    var path = {
                        base:false,
                        source: item,
                        destination: fileName
                    }
                    var fileType = ".js";
                    if(item == "config" || item == "json"){
                        path.base = true;
                    }
                    if(item == "view"){
                        fileType = ".html";
                    } else if(item == "style"){
                        fileType = ".css";
                    } else if(item == "json"){
                        fileType = ".json";
                    }

                    copyTemplate(path, variables, fileType);
                })
            }

            function copyTemplate(path, variables, fileType){
                variables.camelizedName = camelize(variables.appNameWithPrefix);
                var templatePath = (path.base?"":(path.source + "/")) + (path.source=="json"?path.destination.split('.')[1] || "":path.source)  + fileType;
                var destinationPath = ( path.base?"":(path.source + '/')) + path.destination +(path.source=="json"?"":('.' + path.source )) + fileType;
                templatePath = baseTemplatePath + "/templates/app/" + templatePath;
                destinationPath = outputAppPath + "/" + destinationPath;
                copyFile(templatePath,destinationPath,variables);
            }


        }, function (err) {

        })

        function copyi18n() {
            createDirectory(outputAppPath + "/i18n");
            var files = ["lang-de-DE.json", "lang-en-US.json", "lang-fr-FR.json"];
            files.forEach(function (item) {
                var templatePath = baseTemplatePath + "/templates/app/i18n/" + item;
                var destinationPath = outputAppPath + "/i18n/" + item;
                copyFile(templatePath, destinationPath, variables);
            })
        }

    }

    function copyFile(src, dest, variables) {
        var fs = require('fs')
        fs.readFile(src, 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            var result = data;
           for(var key in variables){
               var replace = "<%= " + key +" %>";
               var re = new RegExp(replace,"g");
               result = result.replace(re, variables[key]);
           }
            fs.writeFile(dest, result, 'utf8', function (err) {
                if (err) return console.log(err);
                console.log("Created: " + dest);
            });
        });
    }


    function camelize(str) {
        var stringWithOutDots = str.replace('.', '-');
        return stringWithOutDots
            .replace(/-(.)/g, function ($1) {
                return $1.toUpperCase();
            })
            .replace(/-/g, '')
            .replace(/^(.)/, function ($1) {
                return $1.toLowerCase();
            });
    }

    function getAppSourcePath() {
        var Q = require('q');
        var deferred = Q.defer();

        var cache = require('../providers/node-cache-provider');
        cache.instance().get("connectionDetails", function (err, value) {
            if (!err && value) {
                value.src = value.src.replace(/\\/g, '/');
                value.src = value.src.replace('\\', '/');
                deferred.resolve(value.src);
            } else {
                deferred.reject({
                    status: false,
                    msg: "No connection data available"
                });

            }
        });

        return deferred.promise;
    }

    function createDirectory(dirPath) {
        var fs = require('fs');
        try {
            fs.mkdirSync(dirPath);
            console.log("Created: " + dirPath);
            return 0;
        } catch (err) {
            if (err.code !== 'EEXIST') return 0;
        }
    }


    function getPlatformOrInfrastructureApps(path) {
        var apps = "";
        if (dirOrFileExists(path)) {
            apps = getDirectories(path);
        }
        return apps;
    }

    function getBusinessOrComponentApps(path) {
        var sites = getDirectories(path);
        var apps = {};

        for (var i = 0; i < sites.length; i++) {
            apps[sites[i]] = getDirectories(path + '/' + sites[i]);
        }
        return apps;
    }

    function getDirectories(srcpath) {
        const fs = require('fs');
        const path = require('path');
        return fs.readdirSync(srcpath)
                .filter(function (file){
                    return fs.statSync(path.join(srcpath, file)).isDirectory();
                });
    }

    function dirOrFileExists(path) {
        var fs = require('fs');
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    }
};
