﻿module.exports = function (app, route) {
    return function (req, res, next) {
        storeConnectionDetails(req.body).then(function(response) {
            disconnectMongoDb();
            res.send(response);
            next();
        }, function(error) {
            res.send(error);
            next();
        });
    };

    function storeConnectionDetails(details) {
        var Q = require('q');
        var defered = Q.defer();
        var cache = require('../providers/node-cache-provider');
        cache.instance().set("connectionDetails", details, function (err, success) {
            if (!err && success) {
                defered.resolve(success);
            } else {
                defered.resolve(false);
            }
        });

        return defered.promise;

    }

    function disconnectMongoDb(){
        var mongoose = require('mongoose');
        if (mongoose.connection.readyState) {
            mongoose.connection.close(function(){
                console.log("Disconnected");
            });
        }
    }
};
