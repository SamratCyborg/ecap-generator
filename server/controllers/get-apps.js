﻿module.exports = function (app, route) {
    return function (req, res, next) {
        getApps().then(function (apps) {
            res.send(apps);
            next();
        }, function (err) {
            res.json({
                status: 1,
                msg: "Could not resolve apps"
            });
            next();
        });
    };

    function getAppSourcePath() {
        var Q = require('q');
        var deferred = Q.defer();

        var cache = require('../providers/node-cache-provider');
        cache.instance().get("connectionDetails", function (err, value) {
            if (!err && value) {
                value.src = value.src.replace(/\\/g, '/');
                value.src = value.src.replace('\\', '/');
                deferred.resolve(value.src);
            } else {
                deferred.reject({
                    status: false,
                    msg: "No connection data available"
                });

            }
        });

        return deferred.promise;
    }

    function getApps() {
        var Q = require('q');
        var deferred = Q.defer();

        getAppSourcePath().then(function (res) {
            var appBaseDir = res + (res.charAt(res.length - 1) == '/' ? 'app' : '/app');
            var allApps = {
                "platform": getPlatformOrInfrastructureApps(appBaseDir + '/platform'),
                "business": getBusinessOrComponentApps(appBaseDir + '/business'),
                "component": getBusinessOrComponentApps(appBaseDir + '/component'),
                "infrastructure": getPlatformOrInfrastructureApps(appBaseDir + '/infrastructure')
            };
            deferred.resolve(allApps);
        }, function (err) {
            deferred.reject(err);
        })


        return deferred.promise;
    }

    function getPlatformOrInfrastructureApps(path) {
        var apps = "";
        if (dirOrFileExists(path)) {
            apps = getDirectories(path);

        }
        return apps;
    }

    function getBusinessOrComponentApps(path) {
        var sites = getDirectories(path);
        var apps = {};

        for (var i = 0; i < sites.length; i++) {
            apps[sites[i]] = getDirectories(path + '/' + sites[i]);
        }
        return apps;
    }

    function getDirectories(srcpath) {
        const fs = require('fs');
        const path = require('path');
        return fs.readdirSync(srcpath)
                .filter(function (file){
                    return fs.statSync(path.join(srcpath, file)).isDirectory();
                });
    }



    function dirOrFileExists(path) {
        var fs = require('fs');
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    }
};
