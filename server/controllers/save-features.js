﻿module.exports = function (app, route) {
    return function (req, res, next) {
      
    saveFeatures(req.body).then(function(response) {
        res.send(response);
        next();
    }, function(err) {
        res.send(err);
        next();
    }); 
    };

    function saveFeatures(featureDetails) {
        var Q = require('q');
        var defered = Q.defer();
        connectToDb().then(function (res) {
            getAppSourcePath().then(function (res) {
                var appBaseDir = res + (res.charAt(res.length - 1) == '/' ? 'app' : '/app')+ getAppPath(featureDetails.appDetails);
                var appFeaturesInfo = {};
                var appJsonInfo = {};
                appFeaturesInfo.path = appBaseDir + '/' + featureDetails.appDetails.appName + '.features.json';
                appJsonInfo.path = appBaseDir + '/' + featureDetails.appDetails.appName + '.json';


                appFeaturesInfo.data = readJsonFile(appFeaturesInfo.path);
                appJsonInfo.data = readJsonFile(appJsonInfo.path);

                writeFeatures(appFeaturesInfo, appJsonInfo, featureDetails).then(function(res) {
                    defered.resolve(res);
                }, function(err) {
                    defered.reject(err);
                });
            }, function (err) {
                defered.reject(err);
            })

        }, function(err) {
            defered.reject(err);
        });
        return defered.promise;
    }
    function getAppSourcePath() {
        var Q = require('q');
        var deferred = Q.defer();

        var cache = require('../providers/node-cache-provider');
        cache.instance().get("connectionDetails", function (err, value) {
            if (!err && value) {
                value.src = value.src.replace(/\\/g, '/');
                value.src = value.src.replace('\\', '/');
                deferred.resolve(value.src);
            } else {
                deferred.reject({
                    status: false,
                    msg: "No connection data available"
                });

            }
        });

        return deferred.promise;
    }
    function writeFeatures(appFeaturesInfo, appJsonInfo, featureDetails) {

        var responseObject = {
            status: 0
        }
        
        var Q = require('q');
        var defered = Q.defer();
        var featureIndex = appFeatureIndexer(appFeaturesInfo.data.features);
        var app = featureDetails.appDetails;
        var appType;
        if (app.appType == "platform" || app.appType == "infrastructure") {
            var appType = app.appType
        } else {
            appType = app.appType + '.' + app.siteName
        }
        var entitySignature = {
            AppType: appType,
            AppName: app.appName
        }
        writeToDb(featureDetails.features, entitySignature).then(function(res) {
            for (var i = 0; i < featureDetails.features.length; i++) {
                var item = featureDetails.features[i];
                entitySignature.FeatureId = item.featureId;
                entitySignature.FeatureName = item.featureName ? item.featureName : "";
                if (item.hasDeleted) {
                    appFeaturesInfo.data.features.splice(featureIndex.indexOf(item.featureId), 1);
                    delete appJsonInfo.data.featureMap[item.featureId];
                    featureIndex = appFeatureIndexer(appFeaturesInfo.data.features);
                } else if (item.isNew) {
                    appFeaturesInfo.data.features.push({
                        id: item.featureId,
                        name: item.featureName,
                        desc: item.desc,
                        icon: item.icon
                    });
                    appJsonInfo.data.featureMap[item.featureId] = {
                        views: item.views,
                        url: item.url
                    };
                }
            }

            writeJsonFile(appFeaturesInfo.path, appFeaturesInfo.data).then(function (res) {
                writeJsonFile(appJsonInfo.path, appJsonInfo.data).then(function (res) {
                    defered.resolve({
                        status: 0,
                        msg: "Feature Inserted"
                    });
                }, function (err) {
                    defered.reject(err);
                });
            }, function (err) {
                defered.reject(err);
            });

        }, function(err) {
            defered.reject(err);
        })
        return defered.promise;
    }


    function writeToDb(features, entitySignature) {
        var Q = require('q');
        var defered = Q.defer();
        var promises = [];
        for (var i = 0; i < features.length; i++) {
            var item = features[i];
            if (!Array.isArray(item.roles) || !Array.isArray(item.endpoints)) {
                defered.reject({
                    status: 1,
                    msg: "Invalid feature details provided",
                    Desc: "roles and endpoints must be array"
                })
            }
            
            entitySignature.FeatureId = item.featureId;
            entitySignature.FeatureName = item.featureName;
            var entitySignatureCopy = JSON.parse(JSON.stringify(entitySignature));
           
            var models = prepareModels();
            if (item.hasDeleted) {
                for (var j = 0; j < item.endpoints.length; j++) {
                    var entitySignatureCopy = JSON.parse(JSON.stringify(entitySignature));
                    entitySignatureCopy.ResourcePath = item.endpoints[j].toLowerCase();
                    promises.push(deleteEntity(models.featureEndpointMapModel, entitySignatureCopy));
                   
                }
                for (var j = 0; j < item.roles.length; j++) {
                    var entitySignatureCopy = JSON.parse(JSON.stringify(entitySignature));
                    entitySignatureCopy.RoleName = item.roles[j];
                    promises.push(deleteEntity(models.featureRoleMapModel, entitySignatureCopy));
                }
            } else if (item.isNew) {
                for (var j = 0; j < item.endpoints.length; j++) {
                    var splitedUri = item.endpoints[j].split('/');
                    if (splitedUri.length != 3) {
                        defered.reject({
                            status: 2,
                            msg: "The enpoints provided is invalid"
                        })
                    }
                    var entitySignatureCopy = JSON.parse(JSON.stringify(entitySignature));
                    entitySignatureCopy.ResourcePath = item.endpoints[j].toLowerCase();
                    entitySignatureCopy.Service = splitedUri[0];
                    entitySignatureCopy.Controller = splitedUri[1];
                    entitySignatureCopy.Action = splitedUri[2];
                    entitySignatureCopy.ResourceId = (splitedUri[0] + '-' + splitedUri[1] + '-' + splitedUri[2]);
                    promises.push(saveToMongoDb(models.featureEndpointMapModel, entitySignatureCopy));
                }
                for (var j = 0; j < item.roles.length; j++) {
                    var entitySignatureCopy = JSON.parse(JSON.stringify(entitySignature));
                    entitySignatureCopy.RoleName = item.roles[j];
                    promises.push(saveToMongoDb(models.featureRoleMapModel, entitySignatureCopy));
                }
            } else {
                defered.resolve({
                    status: 3,
                    msg: "Something unexpected happended"
                })
            }
        }
       
       
        Q.all(promises).then(function(res) {
            defered.resolve(res);
        }, function(err) {
            defered.reject(err);
        })
        return defered.promise;
    }

    function deleteEntity(model, data) {
        delete data.FeatureName;
        var Q = require('q');
        var defered = Q.defer();
        model.remove(data, function (err, doc) {
            if (err) {
                console.log("Failed to delete entity");
                console.log(err);
                defered.reject({
                    status: 2,
                    msg: "Could not delete from db"
                });
            } else {
                console.log(doc);
                console.log("Entity deleted");
                defered.resolve({
                    status: 0,
                    msg: "Delete from db successfull"
                });
            }
        });

        return defered.promise;
    }

    function prepareModels() {
        var mongoose = require('mongoose');
        var schema = mongoose.Schema;
        var featureEndpointMapModel;
        var featureRoleMapModel
        var featureEndpointMapSchema = new schema({
            AppType: {
                type: String,
                required: true
            },
            AppName: {
                type: String,
                required: true
            },
            FeatureId: {
                type: String,
                required: true
            },
            FeatureName: {
                type: String
            },
            ResourceId: {
                type: String,
                required: true
            },
            Service: {
                type: String,
                required: true
            },
            Controller: {
                type: String,
                required: true
            },
            Action: {
                type: String,
                required: true
            },
            ResourcePath: {
                type: String,
                required: true
            }
        }, { collection: 'FeatureEndPointMaps' });
        var featureRoleMapSchema = new schema({
            AppType: {
                type: String,
                required: true
            },
            AppName: {
                type: String,
                required: true
            },
            FeatureId: {
                type: String,
                required: true
            },
            FeatureName: {
                type: String
            },
            RoleName: {
                type: String,
                required: true
            }
        }, { collection: 'FeatureRoleMaps' });
        try {
            featureEndpointMapModel = mongoose.model('FeatureEndPointMap')
        } catch (error) {
            featureEndpointMapModel = mongoose.model("FeatureEndPointMap", featureEndpointMapSchema);
        }
        try {
            featureRoleMapModel = mongoose.model('FeatureRoleMap')
        } catch (error) {
            featureRoleMapModel = mongoose.model("FeatureRoleMap", featureRoleMapSchema);
        }
        return {
            featureEndpointMapModel: featureEndpointMapModel,
            featureRoleMapModel: featureRoleMapModel
        }
    }

    function connectToDb() {
        var mongoose = require('mongoose');
        var Q = require('q');
        var defered = Q.defer();
        if (!mongoose.connection.readyState) {
            var cache = require('../providers/node-cache-provider');
            cache.instance().get("connectionDetails", function (err, value) {
                if (!err && value) {
                    // console.log("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName);

                    mongoose.connect("mongodb://" + value.ip + ":" + value.port + "/" + value.dbName, function (err) {
                        if (err) {
                            defered.reject({
                                status: false,
                                msg: "Wrong connection details provided",
                                err: err
                            });
                        } else {
                            defered.resolve({
                                status: true,
                                msg: "Connected to db"
                            });
                        }
                    });
                } else {
                    defered.reject({
                        status: false,
                        msg: "No connection data available"
                    });

                }
            });
        } else {
            defered.resolve({
                status: true,
                msg: "Connected to db"
            });
        }


        return defered.promise;
    }
    function saveToMongoDb(model, entity) {
        var Q = require('q');
        var defered = Q.defer();
        var document = new model(entity);
        connectToDb().then(function (res) {
            document.save(function (err, doc) {
                if (err) {
                    console.log(err);
                    defered.reject({
                        status: 2,
                        msg:"Could not insert to db"
                    });
                } else {
                    console.log("Inserted");
                    console.log(doc);
                    defered.resolve({
                        status: 0,
                        msg: "Inserted to db"
                    });
                }
            });
        }, function (err) {
            console.log(err);
            defered.reject({
                status: 2,
                msg: "Could not insert to db"
            });
        })
        return defered.promise;
    }

    function appFeatureIndexer(features) {
        var indexed = [];
        for (var i = 0; i < features.length; i++) {
            indexed.push(features[i].id);
        }
        return indexed;
    }

    function getAppPath(appDetails) {
        if (appDetails.appType == "platform" || appDetails.appType == "infrastructure") {
            return '/' + appDetails.appType + '/' + appDetails.appName;
        } else if (appDetails.appType == "business" || appDetails.appType == "component") {
            return '/' + appDetails.appType + "/" + appDetails.siteName + '/' + appDetails.appName;
        }
    }

    function readJsonFile(path) {
        var fs = require('fs');
        var rawJsonData = fs.readFileSync(path, 'utf8');
        var jsonData = parseJsonString(rawJsonData);

        if (jsonData) {
            return jsonData;
        } else {
            console.log("Invalid JSON, " + path);
            console.log("Aborting task....");
            throw new Error("Invalid json supplied");
        }
    }

    function parseJsonString(str) {
        var parsedJson;
        try {
            parsedJson = JSON.parse(str.toString().trim());
        } catch (e) {
            return false;
        }
        return parsedJson;
    }

    function writeJsonFile(path, data) {
        console.log("write " + path);
        var Q = require('q');
        var defered = Q.defer();
        var fs = require('fs');
        var json = JSON.stringify(data, null, 4);
        fs.writeFile(path, json, 'utf8', function (err) {
            if (err) {
                console.log("Error writing file " + path);
                defered.reject(err);
            } else {
                defered.resolve("File Writen");
            }
        });
        return defered.promise;
    }

};
