﻿module.exports = function (app, route) {
    return function (req, res, next) {
        getAllEndpoints().then(function (endpoints) {
            res.send(endpoints);
            next();
        });
    };

    function getAllEndpoints() {
        var Q = require('q');
        var defered = Q.defer();
        var microservices;
        getMicroservices().then(function (res) {
            microservices = res;
            var resourceEndpoints = prepareResourceEndpoints(microservices);

            var result = {};

            var promises = [];
            for (var key in resourceEndpoints) {
                promises.push(getEndpoints(resourceEndpoints[key]));
            }


            Q.all(promises).then(function (success) {
                // console.log(success);
                for (var i = 0; i < success.length; i++) {
                    try {
                        var endpoints = JSON.parse(success[i]);
                    } catch (e) {
                        continue;
                    }
                    for (var j = 0; j < endpoints.length; j++) {
                        if (!Array.isArray(result[endpoints[j].Service])) {
                            result[endpoints[j].Service] = [endpoints[j]];
                        } else {
                            result[endpoints[j].Service].push(endpoints[j]);
                        }
                    }
                }
                defered.resolve(result);
            });
        }, function (err) {
            defered.reject(err);
        })
        return defered.promise;
    }

    function getMicroservices() {
        var Q = require('q');
        var deferred = Q.defer();
        getAppSourcePath().then(function (res) {
            var path = res + (res.charAt(res.length - 1) == '/' ? 'shell/boot/environment.dev.js' : '/shell/boot/environment.dev.js');
            var vm = require("vm");
            var fs = require("fs");
            var data = fs.readFileSync(path);
            this.appSuite = {};
            vm.runInNewContext(data, this, path);
            deferred.resolve(this.appSuite.shellApi);
        }, function (err) {
            deferred.reject(err);
        })


        return deferred.promise;
    }

    function getAppSourcePath() {
        var Q = require('q');
        var deferred = Q.defer();

        var cache = require('../providers/node-cache-provider');
        cache.instance().get("connectionDetails", function (err, value) {
            if (!err && value) {
                value.src = value.src.replace(/\\/g, '/');
                value.src = value.src.replace('\\', '/');
                deferred.resolve(value.src);
            } else {
                deferred.reject({
                    status: false,
                    msg: "No connection data available"
                });

            }
        });

        return deferred.promise;
    }

    function prepareResourceEndpoints(microservices) {
        var result = {};
        for (var key in microservices) {
            var splitedUri = microservices[key].split(/\//);
            if (splitedUri[3]) {
                var options = {
                    host: splitedUri[2],
                    path: '/' + splitedUri[3] + '/Management/GetResources'
                };
                result[splitedUri[3].toLowerCase()] = options;
            }
        }

        return result;
    }

    function extractFeatures(appFeatureData, appJsonData, appDetails) {
        var features = [];
        for (var i = 0; i < appFeatureData.length; i++) {
            var feature = {
                "featureId": appFeatureData[i].id,
                "views": appJsonData[appFeatureData[i].id].views,
                "url": appJsonData[appFeatureData[i].id].url,
                "icon": appFeatureData[i].icon
            }
            feature.endpoints = getEndpoints(appDetails, feature);
            feature.roles = getRoles(appDetails, feature);
            features.push(feature);
        }

        return features;
    }

    /* function getEndpoints(options) {
     var http = require('http');
     http.get(options, function(resp){
     resp.setEncoding('utf8');
     resp.on('data', function(chunk){
     console.log(chunk);

     console.log("first jhsdtgkljkl");
     });
     }).on("error", function(e){
     console.log("Got error: " + e.message);
     });
     }*/

    function getEndpoints(options) {
        var Q = require('q');
        var defered = Q.defer();
        var http = require('http');
        var data;
        var endpointRequest = http.get(options, function (endpointResponse) {
            endpointResponse.setEncoding('utf8');
            endpointResponse.on('data', function (chunk) {
                data = chunk;
                defered.resolve(data);
            });
        });

        endpointRequest.on('error', function (e) {
            console.log('ERROR: ' + e.message);
            defered.resolve('not found');
        });
        return defered.promise;
    }

    function getRoles(appDetails, feature) {
        return ["appuser", "anonymous"];
    }

    /*function getAppPath(appDetails) {
        if (appDetails.appType == "platform" || appDetails.appType == "infrastructure") {
            return '/' + appDetails.appType + '/' + appDetails.appName;
        } else if (appDetails.appType == "business" || appDetails.appType == "component") {
            return '/' + appDetails.appType + "/" + appDetails.siteName + '/' + appDetails.appName;
        }
    }

    function getPlatformOrInfrastructureApps(path) {
        var apps = "";
        if (dirOrFileExists(path)) {
            apps = getDirectories(path);
        }
        return apps;
    }

    function getBusinessOrComponentApps(path) {
        var sites = getDirectories(path);
        var apps = {};

        for (var i = 0; i < sites.length; i++) {
            apps[sites[i]] = getDirectories(path + '/' + sites[i]);
        }
        return apps;
    }

    function getDirectories(srcpath) {
        const fs = require('fs');
        const path = require('path');
        return fs.readdirSync(srcpath)
                .filter(file = > fs.statSync(path.join(srcpath, file)).isDirectory()
    )
        ;
    }*/

    function dirOrFileExists(path) {
        var fs = require('fs');
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    }

    function readJsonFile(path) {
        var fs = require('fs');
        var rawJsonData = fs.readFileSync(path, 'utf8');
        var jsonData = parseJsonString(rawJsonData);

        if (jsonData) {
            return jsonData;
        } else {
            console.log("Invalid JSON, " + path);
            console.log("Aborting task....");
            throw new Error("Invalid json supplied");
        }
    }

    function parseJsonString(str) {
        var parsedJson;
        try {
            parsedJson = JSON.parse(str.toString().trim());
        } catch (e) {
            return false;
        }
        return parsedJson;
    }

};
