/*!
	Copyright (C) Secure Link Services AG. <info@selise.ch>
	This file is part of SELISE ECAP (Enterprise Cloud Application Platform)
	Any code from SELISE ECAP can not be copied, distributed, reused, published without explicit written and signed permission of management of Secure Link Services AG.
*/
(function(angular) {
    "use strict";

    var module = angular.module("<%= appNameWithPrefix %>", ["pascalprecht.translate"]);
    function config() {

    }
    config.$inject = [];
    module.config(config);

    function run($translatePartialLoader) {
        $translatePartialLoader.addPart('<%= appUrl %>/i18n');

    }
    run.$inject = ["$translatePartialLoader"];
    module.run(run);

})(window.angular);
