﻿/*!
	Copyright (C) Secure Link Services AG. <info@selise.ch>
	This file is part of SELISE ECAP (Enterprise Cloud Application Platform)
	Any code from SELISE ECAP can not be copied, distributed, reused, published without explicit written and signed permission of management of Secure Link Services AG.
*/

(function (angular, appSuite) {
    "use strict";
  constructor.$inject = ["<%= camelizedName %>Service"];
  angular.module("<%= appNameWithPrefix %>").controller('<%= camelizedName %>Controller', constructor);
    function constructor(<%= camelizedName %>Service) {
        var vm = this;
        vm.$onInit = function () {

        };
    }
})(window.angular, window.appSuite);
