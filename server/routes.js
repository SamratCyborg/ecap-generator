module.exports = {
    '/checkConnection': require('./controllers/check-connection'),
    "/getEndpoints": require('./controllers/get-endpoints'),
    "/setConnectionDetails": require('./controllers/set-connection-details'),
    "/getApps": require('./controllers/get-apps'),
    "/saveFeatures": require('./controllers/save-features'),
    "/getFeatures": require('./controllers/get-features'),
    "/createNewApp": require('./controllers/create-new-app')   
};
