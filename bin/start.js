#! /usr/bin/env node
var shell = require("shelljs");
var concurrently = require('concurrently');
var open = require('open');
var npmPath = shell.exec("npm config get prefix");
var path = require('path');

var s = npmPath.split(path.sep);
var basePath  = "";

for (var i=0;i< (s.length-1);i++){
basePath += (s[i] + "\\")
}
basePath += "npm"

var serverPath = path.resolve(basePath + "\\node_modules\\ecap-generator\\server\\index");
var clientPath = path.resolve(basePath+ "\\node_modules\\ecap-generator\\client");

open('http://127.0.0.1:5050');
shell.exec("concurrently \"node "+serverPath+"\" \"http-server "+clientPath+" -p 5050\"");