﻿(function (angular) {
	"use strict";
	function constructor($scope, $mdDialog, rfmService) {
		$scope.submit = function (appType, appName, siteName) {
			$mdDialog.hide({
				appType: appType,
				appName: appName,
				siteName: siteName
			});
		};

		$scope.cancel = function () {
			$mdDialog.hide();
		};

		function init(){
			$scope.newAppDetails = rfmService.getNewAppDetails();
			console.log($scope.newAppDetails);
		}
		init();
	}

	constructor.$inject = ["$scope", "$mdDialog", "rfmService"];
	angular.module("rfmg").controller('newAppDialogController', constructor);

})(window.angular);