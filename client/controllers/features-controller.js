﻿(function (angular) {
    "use strict";
    function constructor($scope,$rootScope, rfmService) {
        $scope.test = "ECAP Role Feature Map Generator";
        $scope.data = [];


        function init() {
           
        }
        $scope.addNewFeature = function() {
            $rootScope.$broadcast("new-feature-add");
        }

        $rootScope.$on("new-app-selected", function() {
            rfmService.getFeatures().then(function (response) {
                //$scope.features = JSON.stringify(response, null, 4);
                $scope.treeDataFeature = new kendo.data.HierarchicalDataSource(buildUiTreeData(response));
                $scope.appName = rfmService.getSelectedApp().appName;
            });
        });
        $rootScope.$on("connection-refreshed", function(){
            $scope.treeDataFeature = null;
            $scope.appName = null;
        });
        function buildUiTreeData(data) {
            var result = {
                data: [
                ]
            };
            for (var i = 0; i < data.length; i++) {
                result.data.push({
                    text: data[i].featureId,
                    featureDetails: JSON.stringify(data[i])
                });
            }
            return result;
        }

        $scope.onSelect = function (data) {
            //console.log(data);
            rfmService.setSelectedFeature(data.featureDetails);
        }

        init();
    }

    constructor.$inject = ["$scope","$rootScope", "rfmService"];
    angular.module("rfmg").controller('featuresController', constructor);
    
})(window.angular);