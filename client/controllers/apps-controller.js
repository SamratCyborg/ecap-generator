(function (angular) {
    "use strict";
    function constructor(  $rootScope,$scope, $mdDialog, $mdToast, rfmService) {
        $scope.test = "ECAP Role Feature Map Generator";
        $scope.data = [];


        function init() {
            rfmService.getAllApps().then(function (response) {
                $scope.apps = response;
                $scope.treeData = new kendo.data.HierarchicalDataSource(buildUiTreeData(response));
            });
        }

        $rootScope.$on("connection-refreshed", function(){
            init();
        })

        function buildUiTreeData(rawData) {
            var result = {
                data: []
            };
            for (var item in rawData) {
                if (item == "platform" || item == "infrastructure") {
                    result.data.push({
                        text: item,
                        items: getKendoTreeList(rawData[item], item)
                    });
                } else if (item == "business" || item == "component") {
                    result.data.push({
                        text: item,
                        items: getKendoTreeListForBusinessApp(rawData[item], item)
                    });
                }
            }

            return result;
        }

        function getKendoTreeList(data, appType, siteName) {
            var result = [];
            for (var i = 0; i < data.length; i++) {
                result.push({
                    text: data[i],
                    appType: appType,
                    siteName: siteName,
                    appName: data[i]
                });
            }
            return result;
        }

        function getKendoTreeListForBusinessApp(data, appType) {
            var result = [];
            for (var item in data) {
                result.push({
                    text: item,
                    items: getKendoTreeList(data[item], appType, item)
                });
            }
            return result;
        }

        $scope.showFeature = function (data) {
            rfmService.setSelectedApp({
                appName: data.appName,
                appType: data.appType,
                siteName: data.siteName
            });
        }

        var newAppDetails = {};
        $scope.addNew = function (data) {

            console.log(data);
            if (data.text == "business" || data.text == "component") {
                return;
            } else if (data.text == "infrastructure" || data.text == "platform") {
                newAppDetails.newAppType = data.text;
                showDialog();
            } else {
                if (data.children && data.children.options && data.children.options.data
                    && data.children.options.data.items && data.children.options.data.items[0]) {
                    newAppDetails.newAppType = data.children.options.data.items[0].appType;
                    newAppDetails.newAppSiteName = data.text;
                    showDialog();
                } else {
                    newAppDetails.newAppType = "business";
                    newAppDetails.newAppSiteName = data.text;
                    showDialog();
                }
            }

            rfmService.setNewAppDetails(newAppDetails);

        }

        function showDialog() {
            $mdDialog.show({
                    controller: "newAppDialogController",
                    templateUrl: 'views/new-app-details-dialog.view.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: true
                })
                .then(function (answer) {
                    console.log(answer);
                    rfmService.createNewApp(answer).then(function (res) {
                        showToast("New app created");
                    }, function (err) {
                        showToast("App creating failed");
                    });
                }, function () {
                    return false;
                });
        }

        function showToast(msg) {
            $mdToast.show($mdToast.simple()
                .content(msg)
                .position("top right")
                .hideDelay(1000));
        }

        $scope.onSelect = function (data) {
            $scope.addChild = false;
            $scope.seeFeature = false;
            if (data.hasChildren) {
                $scope.addChild = true;
            } else {
                $scope.showFeature(data);
            }
        }

        init();
    }

    constructor.$inject = ["$rootScope","$scope", "$mdDialog", "$mdToast", "rfmService"];
    angular.module("rfmg").controller('appsController', constructor);

})(window.angular);
