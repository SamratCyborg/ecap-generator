(function (angular) {
    "use strict";
    function constructor($scope, $rootScope, $timeout, rfmService, $mdToast) {
        $scope.test = "ECAP Role Feature Map Generator";
        $scope.data = [];
        $scope.code = "ECAP Role Feature Map Generator";

        function init() {

        }

        $scope.aceOptions = {
            method: 'sql',
            theme: 'sqlserver',
            onLoad: function (editor, session, ace) {
                console.log(editor);
                console.log(session);
                console.log(ace);

            }
        }


        $scope.removeEndpoint = function (index) {
            $scope.endpointList.splice(index, 1);
        }
        $scope.removeRole = function (index) {
            $scope.roleList.splice(index, 1);
        }


        $rootScope.$on("new-feature-selected", function () {
            $scope.showFeature = true;
            var selectedFeature = JSON.parse(rfmService.getSelectedFeature());
            $scope.endpointList = selectedFeature.endpoints;
            $scope.roleList = selectedFeature.roles;
            delete selectedFeature.endpoints;
            delete selectedFeature.roles;
            $scope.editMode = false;
            $scope.featureModel = JSON.stringify(selectedFeature, null, 4);
        });
        $rootScope.$on("new-app-selected", function () {
            $scope.featureModel = null;
            $scope.editMode = false;
            $scope.showFeature = false;
        });
        $rootScope.$on("connection-refreshed", function(){
            $scope.featureModel = null;
            $scope.editMode = false;
            $scope.showFeature = false;
        });
        $rootScope.$on("new-feature-add", function () {
            $scope.showFeature = true;
            addNewFeature();
        });

        function addNewFeature() {
            rfmService.setFeatureToBeEdited(true);
            $scope.newFeature = true;
            $scope.roleList = [];
            $scope.endpointList = [];
            $scope.editMode = true;
            var model = {
                "featureId": "",
                "featureName": "",
                "views": [{
                    "@": ""
                }],
                "url": ""
            };
            $scope.featureModel = JSON.stringify(model, null, 4);
        }

        $scope.saveFeature = function (featureModel) {
            // return;
            var data = [];
            var newFeature;
            var oldFeature = rfmService.getFeatureToBeEdited();
            if(oldFeature){
                var old = parseJson(oldFeature);
                if (!old) return;
                old.hasDeleted = true;
                old.isNew = false;
                data.push(old);
            }
            newFeature = parseJson(featureModel);
            if(newFeature)
            {
                newFeature.endpoints = $scope.endpointList || [];
                newFeature.roles = $scope.roleList || [];
                if (!validateFeatureModel(newFeature)) {
                    showToast("Feature model is invalid");
                    return;
                } else {
                    newFeature.isNew = true;
                    newFeature.hasDeleted = false;
                    data.push(newFeature);
                }
            }

            rfmService.saveFeature(data).then(function (res) {
                if (res.status == 0) {
                    showToast("Feature inserted successfully");
                    $scope.editMode = false;
                    delete newFeature.isNew;
                    delete newFeature.hasDeleted;
                    $scope.roleList = newFeature.roles;
                    rfmService.setSelectedFeature(JSON.stringify(newFeature, null, 4));
                    delete  newFeature.roles;
                    $scope.endpointList = newFeature.endpoints;
                    delete newFeature.endpoints;
                    $scope.featureModel = JSON.stringify(newFeature, null, 4);
                }
            });
        }

        $scope.applyModel = function (txt) {
            $scope.featureModel = txt;
        }

        function validateFeatureModel(model) {
            if (model.featureId  && model.views) {
                return true;
            } else {
                return false;
            }
        }

        function parseJson(data) {
            var parsedJson;
            try {
                parsedJson = JSON.parse(data);
            } catch (e) {
                showToast("The json supllied for feature is invalid");
                return false;
            }
            return parsedJson;
        }

        function showToast(msg) {
            $mdToast.show($mdToast.simple()
                .content(msg)
                .position("top right")
                .hideDelay(500));
        }

        $scope.doubleClick = function () {
            $timeout(function () {
                $scope.editMode = true;
                $scope.newFeature = false;
                rfmService.setFeatureToBeEdited();

            });
        };

        $scope.singleClick = function () {
            if ($scope.clicked) {
                $scope.cancelClick = true;
                return;
            }

            $scope.clicked = true;

            $timeout(function () {
                if ($scope.cancelClick) {
                    $scope.cancelClick = false;
                    $scope.clicked = false;
                    return;
                }

                //do something with your single click here

                //clean up
                $scope.cancelClick = false;
                $scope.clicked = false;
            }, 500);
        };


        $scope.onSelect = function (data) {
            console.log(data);
        }
        $scope.showAddEndpoint = true;
        $scope.showAddRole = true;
        $scope.hideAddEndpointButton = function () {
            $scope.showAddEndpoint = false;
        }
        $scope.addNewEndpoint = function(keyEvent) {
            if (keyEvent.which === 13)
            {
                $scope.endpointList.push($scope.newEndpoint);
                $scope.showAddEndpoint = true;
            }
        }
        $scope.hideAddRoleButton = function () {
            $scope.showAddRole = false;
        }
        $scope.addNewRole = function(keyEvent) {
            if (keyEvent.which === 13)
            {
                $scope.roleList.push($scope.newRole);
                $scope.showAddRole = true;
            }
        }
        init();
    }

    constructor.$inject = ["$scope", "$rootScope", "$timeout", "rfmService", "$mdToast"];
    angular.module("rfmg").controller('editFeatureController', constructor);

})(window.angular);
