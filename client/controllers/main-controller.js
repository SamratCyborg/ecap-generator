﻿(function (angular) {
    "use strict";
    function constructor($scope, $rootScope, dbService) {
        $scope.title = "ECAP Role Feature Map Generator";
        $scope.refreshConnectionData = function(){
            dbService.setConnectionDetails();
        }
    }

    constructor.$inject = ["$scope", "$rootScope", "dbService"];
    angular.module("rfmg").controller('mainController', constructor);

})(window.angular);
