﻿(function (angular) {
	"use strict";
	function constructor($scope, $mdDialog, dbService) {

		$scope.details = dbService.getConnectionInfo();
		$scope.submit = function (src,ip, port, name) {
			$mdDialog.hide({
				src:src,
				ip: ip,
				port: port,
				dbName: name
			});
		};

		$scope.cancel = function () {
			$mdDialog.hide();
		};
	}

	constructor.$inject = ["$scope", "$mdDialog", "dbService"];
	angular.module("rfmg").controller('dialogController', constructor);

})(window.angular);
