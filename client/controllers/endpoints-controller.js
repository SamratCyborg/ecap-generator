﻿(function (angular) {
    "use strict";
    function constructor($rootScope, $scope,$mdToast, rfmService) {
        $scope.test = "ECAP Role Feature Map Generator";
        $scope.data = [];


        function init() {
            getEndpoints();
        }


        $rootScope.$on("connection-refreshed", function(){
            getEndpoints();
        })


        function getEndpoints(){
            setTimeout(function () {
                if($rootScope.okTolaunch){
                    rfmService.getEndpoints().then(function (response) {
                        $scope.endpoints = response;
                        $scope.treeData = new kendo.data.HierarchicalDataSource(buildUiTreeData(response));
                    });
                } else{
                    getEndpoints();
                }
            }, 500);
        }

        function buildUiTreeData(rawData) {
            var result = {
                data: [
                ]
            };
            for (var item in rawData) {
                result.data.push({
                    text: item,
                    items: getKendoTreeList(rawData[item], item)
                });
            }

            return result;
        }

        function getKendoTreeList(data, appType, siteName) {
            var result = [];
            for (var i = 0; i < data.length; i++) {
                result.push({
                    text: data[i].ResourceId.replace(/-/g,'/')
                });
            }
            return result;
        }
        function showToast(msg) {
            $mdToast.show($mdToast.simple()
                                   .content(msg)
                                   .position("top right")
                                   .hideDelay(500));
        }
        $scope.copyToClipboard = function (data) {
            clipboard.copy({
                'text/plain': data,
                'text/html': data
            }).then(
           function () {
               showToast("Copied to clipboard");
           },
           function (err) { console.log(err);});
        }
        function getKendoTreeListForBusinessApp(data, appType) {
            var result = [];
            for (var item in data) {
                result.push({
                    text: item ,
                    items: getKendoTreeList(data[item], appType, item)
                });
            }
            return result;
        }

        init();
    }

    constructor.$inject = ["$rootScope","$scope","$mdToast", "rfmService"];
    angular.module("rfmg").controller('endpointsController', constructor);

})(window.angular);
