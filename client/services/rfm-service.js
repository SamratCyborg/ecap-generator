﻿(function (angular) {
    "use strict";

    function constructor($http, $q, $rootScope, dbService, $mdToast) {
        var selectedApp;
        var selectedFeature;
        var currentAppFeatures;
        var newAppToBeCreated;
        var featureToBeEdited;
        var baseUrl = "http://127.0.0.1:3000/";

        function saveFeature(features) {
            var data = {
                appDetails: selectedApp,
                features: features
            }
            var defered = $q.defer();
            var url = baseUrl + "saveFeatures";
            sendPostRequest(url, data).then(function (response) {
                $rootScope.$broadcast("new-app-selected");
                defered.resolve(response);
            }, function (err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }

        function getAllApps() {
            var defered = $q.defer();
            var url = baseUrl + "getApps";
            var data = {};
            sendPostRequest(url, data).then(function (response) {
                defered.resolve(response);
            }, function (err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }

        function getFeatures() {
            if (!selectedApp) return;
            var defered = $q.defer();
            var url = baseUrl + "getFeatures";
            sendPostRequest(url, selectedApp).then(function (response) {
                currentAppFeatures = response;
                defered.resolve(response);
            }, function (err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }

        function getEndpoints() {
            var defered = $q.defer();
            var url = baseUrl + "getEndpoints";
            sendPostRequest(url, {}).then(function (response) {
                defered.resolve(response);
            }, function (err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }

        function sendPostRequest(url, data) {
            var defered = $q.defer();
            dbService.checkConnection().then(function (res) {
                $http({
                    url: url,
                    method: "POST",
                    data: data,
                    headers: {'Content-Type': 'application/json'}
                })
                    .then(function (response) {
                            defered.resolve(response.data);
                        },
                        function (err) {
                            defered.reject(err);
                        });
            }, function (err) {
                showConnectionError();
            });


            return defered.promise;
        }

        function createNewApp(appDetails) {
            var defered = $q.defer();
            sendPostRequest(baseUrl + "createNewApp", appDetails).then(function (res) {
                defered.resolve(res);
            }, function (err) {
                defered.reject(err);
            });
            return defered.promise;
        }

        function showConnectionError() {
            $mdToast.show($mdToast.simple()
                .content("Could not connect to db")
                .position("bottom right")
                .hideDelay(1000));
        }

        function setSelectedApp(app) {
            if (selectedApp != app) {
                selectedApp = app;
                $rootScope.$broadcast("new-app-selected");
            }

        }

        function getSelectedApp(app) {
            return selectedApp;
        }

        function getSelectedFeature() {
            return selectedFeature;
        }

        function getCurrentAppFeatures() {
            return currentAppFeatures;
        }

        function setSelectedFeature(feature) {
            selectedFeature = feature;
            $rootScope.$broadcast("new-feature-selected");
        }

        function setNewAppDetails(app){
            newAppToBeCreated = app;
        }
        function getNewAppDetails(){
            return newAppToBeCreated;
        }
        function setFeatureToBeEdited(flag){
            if(flag){
                featureToBeEdited = null;
            }else{
                featureToBeEdited = selectedFeature;
            }

        }
        function getFeatureToBeEdited(){
            return featureToBeEdited;
        }

        this.getAllApps = getAllApps;
        this.getFeatures = getFeatures;
        this.setSelectedApp = setSelectedApp;
        this.getSelectedApp = getSelectedApp;
        this.getSelectedFeature = getSelectedFeature;
        this.setSelectedFeature = setSelectedFeature;
        this.getCurrentAppFeatures = getCurrentAppFeatures;
        this.saveFeature = saveFeature;
        this.getEndpoints = getEndpoints;
        this.createNewApp = createNewApp;
        this.setNewAppDetails = setNewAppDetails;
        this.getNewAppDetails = getNewAppDetails;
        this.setFeatureToBeEdited = setFeatureToBeEdited;
        this.getFeatureToBeEdited = getFeatureToBeEdited;

    }

    constructor.$inject = ["$http", "$q", "$rootScope", "dbService", "$mdToast"];
    angular.module("rfmg").service('rfmService', constructor);
})(window.angular);
