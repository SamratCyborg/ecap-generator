(function (angular) {
    "use strict";

    function constructor($http, $q, $mdDialog, $rootScope) {
        var baseUrl = "http://127.0.0.1:3000/";
        var connectionCheckInprogress = false;
        var connectionDetails;

        function checkConnection() {
            var defered = $q.defer();
            waitForConnectionResolve().then(function (res) {
                connectionCheckInprogress = true;
                sendPostRequest(baseUrl + "checkConnection").then(function (res) {
                    if (res.status) {
                        $rootScope.okTolaunch = true;
                    }

                    if (!res.status) {
                        setConnectionDetails().then(function (res) {
                            connectionCheckInprogress = false;
                            $rootScope.okTolaunch = true;
                            defered.resolve(res);
                        }, function (err) {
                            connectionCheckInprogress = false;
                            defered.reject(err);
                        });
                    } else {
                        connectionCheckInprogress = false;
                        defered.resolve(res);
                    }
                }, function (err) {
                    connectionCheckInprogress = false;
                    defered.reject(err);
                });
            });
            return defered.promise;
        }

        var promises;
        var flag = 0;

        function waitForConnectionResolve() {
            var deferred = $q.defer();
            if (flag == 0) {
                promises = [];
                flag++;
            }
            if (connectionCheckInprogress) {
                setTimeout(function () {
                    if (connectionCheckInprogress) {
                        promises.push(waitForConnectionResolve());
                        flag++;
                    } else {
                        $q.all(promises).then(function () {
                            deferred.resolve(0);
                        })
                    }
                }, 500)
            } else {
                if (flag > 1) {
                    $q.all(promises).then(function () {
                        deferred.resolve(0);
                    })
                } else {
                    deferred.resolve(0);
                }
            }

            return deferred.promise;
        }

        function sendConnectionInfo(info) {
            connectionDetails = info;
            var defered = $q.defer();
            sendPostRequest(baseUrl + "setConnectionDetails", info).then(function (res) {
                if (res) {
                    defered.resolve(res);
                } else {
                    defered.reject(false);
                }
            }, function (err) {
                defered.reject(err);
            });
            return defered.promise;
        }

        function setConnectionDetails() {
            var defered = $q.defer();
            showDialog().then(function (res) {
                sendConnectionInfo(res).then(function (res) {
                    $rootScope.$broadcast("connection-refreshed");
                    defered.resolve(res);
                }, function (err) {
                    defered.reject(err);
                });
            }, function (err) {
                defered.reject(err);
            });
            return defered.promise;
        }

        function showDialog() {
            var defered = $q.defer();
            $mdDialog.show({
                    controller: "dialogController",
                    templateUrl: 'views/connection-details-dialog.view.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: true
                })
                .then(function (answer) {
                    defered.resolve(answer);
                }, function () {
                    defered.reject(false);
                });
            return defered.promise;
        }

        function sendPostRequest(url, data) {
            var defered = $q.defer();
            $http({
                url: url,
                method: "POST",
                data: data,
                headers: {'Content-Type': 'application/json'}
            })
                .then(function (response) {
                        defered.resolve(response.data);
                    },
                    function (err) {
                        defered.reject(err);
                    });

            return defered.promise;
        }

        function getConnectionInfo() {
            return connectionDetails;
        }

        this.checkConnection = checkConnection;
        this.setConnectionDetails = setConnectionDetails;
        this.getConnectionInfo = getConnectionInfo;
    }

    constructor.$inject = ["$http", "$q", "$mdDialog", "$rootScope"];
    angular.module("rfmg").service('dbService', constructor);
})(window.angular);
