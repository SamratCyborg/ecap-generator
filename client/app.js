﻿(function (angular) {
    "use strict";

    var module = angular.module('rfmg', ['ngMaterial', 'ngMdIcons', 'ui.bootstrap', "kendo.directives"]);
    function config() {

    }
    config.$inject = [];
    module.config(config);

    function run() {
        console.log("Module running!");
    }
    run.$inject = [];
    module.run(run);

    module.directive('jsonText', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {
                debugger;
                function into(input) {
                    console.log(JSON.parse(input));
                    return JSON.parse(input);
                }
                function out(data) {
                    return JSON.stringify(data);
                }
                ngModel.$parsers.push(into);
                ngModel.$formatters.push(out);
            }
        };
    });

})(window.angular);