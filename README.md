# ECAP generator #
To generate various ecap component use this generator.

## How to start ##
* Open command prompt in any directory
* Install ecap-generator
```
npm install ecap-generator -g

```
* Open command line from anywhere
* Run ecap-generator
```
st-ecap-gen

```