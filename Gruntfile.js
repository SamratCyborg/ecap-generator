module.exports = function(grunt) {
    var sourcesClient = [
        "client/**/*.js",
        "!client/node_modules/**/*.js",
        "!client/lib/**/*.js",
    ];
    var sourcesServer = [
        "server/**/*.js",
        "!server/node_modules/**/*.js",
        "!server/templates/**/*.js"
    ]

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            distClient: {
                src: sourcesClient,
                dest: 'dist/client/<%= pkg.name %>.js'
            },
            distServer: {
                src: sourcesServer,
                dest: 'dist/server/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/client/<%= pkg.name %>.min.js': ['<%= concat.distClient.dest %>'],
                    'dist/server/<%= pkg.name %>.min.js': ['<%= concat.distServer.dest %>']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['concat:distClient','concat:distServer', 'uglify']);

};